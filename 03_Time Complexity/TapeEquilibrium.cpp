// Codility
// TapeEquilibrium
// time complexity O(n)
// ~ my solution

#include <iostream>
#include <vector>
#include <limits>

using namespace std;

int solution(vector<int> &);

int main() {
   // test example
   vector<int> arr { 3, 1, 2, 4, 3 };

   cout << solution(arr) << endl;

  return 0;
}

int solution(vector<int> &A) {
   int total = 0, lsum = 0, min, absolute;
   // sum of all elements
   for (auto x : A)
      total += x;

   min = numeric_limits<int>::max(); // maximum integer value
   for (int i = 0; i < A.size() - 1; i++) {
      absolute = abs((lsum += A[i]) - (total -= A[i]));
      if (absolute < min)
         min = absolute;
   }

   return min;
}
