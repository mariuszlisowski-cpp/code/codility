// Codility
// Binary Gap
// ~ finds the longest gap of 0's in binary representation of an integer
// ~ my solution

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

string toBinary(int);

int main() {
  int n;
  cin >> n;

  string binary = toBinary(n);

  int max = 0, count = 0;
  for (int i = 0; i < binary.size(); i++) {
    if (binary[i] == '0') {
      if (count > max )
        max = count; // longest sequence so far
      count = 0;
    }
    else {
      count++;
    }
  }

  cout << binary << endl;
  cout << max << endl;

  return 0;
}

string toBinary(int n) {
  string s;
  while (n != 0) {
    s = (n % 2 == 0 ? "0" : "1") + s;
    n /= 2;
  }
  return s;
}
